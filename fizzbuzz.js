class fizzbuzz {
	
	constructor(fizz,buzz,length){
		this.fizz = fizz;
		this.buzz = buzz;
		this.length = length;
	}

	output() {
		for(let i = 0; i < this.length; i++){
			console.log(this.getFizzBuzz(i));
		}
	}

	getFizzBuzz(i) {
		if(i % this.fizz == 0 && i % this.buzz == 0 )
			return 'fizzbuzz'
		else 
			return i % this.fizz == 0 ? 'fizz' : 'buzz';
	}
}

new fizzbuzz(3,5,100).output();